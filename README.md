# disktest
Trying out a few ways to measure disk performance - work is currently
very much in progress!

Output results as a reference are at: https://www.consultron.com/~sfb/disktest/


There's a lot of good info on fio out there, some of the links I've used are:

https://discuss.aerospike.com/t/benchmarking-ssds-with-flexible-io-tester-fio/2788
https://wiki.mikejung.biz/Benchmarking#Fio_Test_Options_and_Examples
https://github.com/axboe/fio/blob/master/HOWTO
http://tfindelkind.com/2015/08/04/fio-flexible-io-tester-part1-installation-and-compiling-if-needed/



### Provisioning a OpenStack VM for a test run ###

These are some notes taken during an actual run - sorry for not fully adapted the new openstack CLI way of doing things. ;)

~~~~
# openstack image create --disk-format qcow2 --container-format bare --public --file xenial-server-cloudimg-amd64-fio.img xenial-cloud-fio
# neutron net-create fionet1
# neutron subnet-create fionet1 192.168.123.0/24 --name fionet1sub
# neutron net-list
# openstack security group create sg_ssh
# openstack security group rule create --proto tcp --dst-port 22:22 --src-ip 0.0.0.0/0 sg_ssh
# nova boot --flavor m1.small --nic net-id=<net-id> --image xenial-cloud-fio --security_group sg_ssh fiovm
# openstack volume create --size 150 fiovol
# nova list
# openstack volume list
# openstack server add volume <vm-id> <volume-id> --device /dev/vdb
# ip netns exec qdhcp-<namespace> ssh root@192.168.23.<n>
~~~~

Hopefully we're now connected to the VM and having /dev/vd<x> available.
