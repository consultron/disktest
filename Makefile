.PHONY: all measure graph clean checkdeps

all:	measure graph

measure: checkdeps
	mkdir -p logs
	fio scenario1.fio | tee logs/scenario1.log

graph: checkdeps
	cd logs && ../fio_generate_plots Measurements

clean:
	rm -f logs/* random1.log *.raw fio_generate_plots

checkdeps:
	./check_deps.sh
