#!/bin/bash

if ! FIO_VERS=$(fio --version 2>/dev/null); then
    echo "I couldn't locate fio in your path. You need to fix this!"
    exit 1
fi

if ! GNUPLOT_VERS=$(gnuplot --version 2>/dev/null); then
    echo "I couldn't locate gnuplot in your path. You need to fix this!"
    exit 1
fi

if [ ! -f fio_generate_plots ]; then
    echo "Patching fio_generate_plots..."
    cp $(which fio_generate_plots) .
    patch -p0 < fio_generate_plots.patch
fi
exit 0

